const Title = ({ titleText }) => {
  return <h2 className="titleText">{titleText}</h2>;
};

export default Title;
