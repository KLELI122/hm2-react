const CardCompetence = ({ src, title, desc }) => {
  return (
    <div className="card competenceCard">
      <div className="headerCard">
        <img className="iconCard" src={src} alt="competence" />
        <h3>{title}</h3>
      </div>

      <ul>
        {desc.map((item) => {
          return <li>{item}</li>;
        })}
      </ul>
    </div>
  );
};

export default CardCompetence;
