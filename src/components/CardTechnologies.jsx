const CardTechnologies = ({ src, name }) => {
  return (
    <div className="card technologyCard">
      <div className="headerCard">
        <img className="iconCard" src={src} alt="technology" />
        <p>{name}</p>
      </div>
    </div>
  );
};

export default CardTechnologies;
