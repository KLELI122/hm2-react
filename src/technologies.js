import animationIcon from "./assets/animation-icon.svg";
import typescriptIcon from "./assets/typescript-icon.svg";
import nextIcon from "./assets/next-icon.svg";
import restIcon from "./assets/rest-icon.svg";
import websocketIcon from "./assets/websocket-icon.svg";
import dockerIcon from "./assets/docker-icon.svg";
import ciIcon from "./assets/ci-icon.svg";
import mobxIcon from "./assets/mobx-icon.svg";
import vueIcon from "./assets/vue-icon.svg";
import patternIcon from "./assets/pattern-icon.svg";

const technologies = [
  {
    id: 0,
    icon: animationIcon,
    name: "Анимации",
  },
  {
    id: 1,
    icon: typescriptIcon,
    name: "TypeScript",
  },
  {
    id: 2,
    icon: nextIcon,
    name: "Next.js",
  },
  {
    id: 3,
    icon: restIcon,
    name: "RestApi",
  },
  {
    id: 4,
    icon: websocketIcon,
    name: "WebSocket",
  },
  {
    id: 5,
    icon: dockerIcon,
    name: "Docker",
  },
  {
    id: 6,
    icon: ciIcon,
    name: "CI/CD",
  },
  {
    id: 7,
    icon: mobxIcon,
    name: "Mobx",
  },
  {
    id: 8,
    icon: vueIcon,
    name: "Vue.js",
  },
  {
    id: 9,
    icon: patternIcon,
    name: "Patterns",
  },
];

export default technologies;
