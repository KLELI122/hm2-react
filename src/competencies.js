import htmlIcon from "./assets/html-icon.svg";
import cssIcon from "./assets/css-icon.svg";
import jsIcon from "./assets/js-icon.svg";
import othersIcon from "./assets/others-icon.svg";

const competencies = [
  {
    id: 0,
    icon: htmlIcon,
    title: "HTML",
    competencies: ["Синтаксис", "Семантическая верстка", "Верстка под React"],
  },
  {
    id: 1,
    icon: cssIcon,
    title: "CSS",
    competencies: ["Позиционированние", "Адаптивная верстка", "Препроцессоры"],
  },
  {
    id: 2,
    icon: jsIcon,
    title: "JavaScript",
    competencies: ["React", "StateManagment", "Unit-тесты"],
  },
  {
    id: 3,
    icon: othersIcon,
    title: "Others",
    competencies: ["Database", "GIT", "Python"],
  },
];

export default competencies;
