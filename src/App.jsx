import "./App.css";
import personImage from "./assets/person-img.jpg";
import competencies from "./competencies";
import technologies from "./technologies";
import Title from "./components/Title";
import Image from "./components/Image";
import CardCompetence from "./components/CardCompetence";
import CardTechnologies from "./components/CardTechnologies";

function App() {
  return (
    <>
      <div className="container">
        <div className="container panel profilePanel">
          <Image className="profileImage" src={personImage} alt="profile" />
          <Title titleText="Клинг Елизавета Евгеньевна" />
        </div>
        <div className="container panel">
          <div className="container competencePanel">
            {competencies.map((item) => {
              return (
                <CardCompetence
                  key={item.id}
                  src={item.icon}
                  title={item.title}
                  desc={item.competencies}
                />
              );
            })}
          </div>
        </div>
      </div>
      <div className="container panel">
        <Title titleText="Технологии, которые я бы хотела изучить" />
        <div className="container technologiesPanel">
          {technologies.map((item) => {
            return (
              <CardTechnologies
                key={item.id}
                src={item.icon}
                name={item.name}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default App;
